%%%                     SISTEMAS INTELIGENTES                           %%%
%%%               CLASIFICADOR DE MOVIMIENTOS DE EMG                    %%%
%%%                RONALD RODRÍGUEZ - JHONATHAN SORA                    %%%
%%%                         3 DE ABRIL 2018                             %%%
%%%                  REDES NEURONALES ARTIFICIALES                      %%%


clc;clear;close all
rutaActual= pwd;    
inicializarRutasBase(rutaActual);
datosPorDefecto = cargarDatosSujetosPorDefecto();
caracteristicas = extraerCaracteristicas(datosPorDefecto);
datos = codificarCampoDeMovimiento(caracteristicas.datos,13);
algoritmo = 'trainscg';

%Escenario 1
camposAUtilizar = [1,2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,1);
set = subSet;
%Escenario 2
camposAUtilizar = [2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,2 );
set = vertcat(set,subSet);
%Escenario 3
camposAUtilizar = [3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,3 );
set = vertcat(set,subSet);
%Escenario 4
camposAUtilizar = [4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,4 );
set = vertcat(set,subSet);
%Escenario 5
camposAUtilizar = [5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,5 );
set = vertcat(set,subSet);
%Escenario 6
camposAUtilizar = [6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,6 );
set = vertcat(set,subSet);
%Escenario 7
camposAUtilizar = [7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,7 );
set = vertcat(set,subSet);
%Escenario 8
camposAUtilizar = [8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,8 );
set = vertcat(set,subSet);
%Escenario 9
camposAUtilizar = [9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,9 );
set = vertcat(set,subSet);
%Escenario 10
camposAUtilizar = [10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,10 );
set = vertcat(set,subSet);
%Escenario 11
camposAUtilizar = [11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,11 );
set = vertcat(set,subSet);
%Escenario 12
camposAUtilizar = 12;
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,12 );
set = vertcat(set,subSet);
%Escenario 13
camposAUtilizar = [5,6,7,8];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,13 );
set = vertcat(set,subSet);
%Escenario 14
camposAUtilizar = [1,2,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,14 );
set = vertcat(set,subSet);
%Escenario 15
camposAUtilizar = [3,4,9,10];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,15 );
set = vertcat(set,subSet);

filename = 'resultadosData_trainscg1.xlsx';
xlswrite(filename,set)

clear('set')
algoritmo = 'trainlm';

%Escenario 1
camposAUtilizar = [1,2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,1);
set = subSet;
%Escenario 2
camposAUtilizar = [2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,2 );
set = vertcat(set,subSet);
%Escenario 3
camposAUtilizar = [3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,3 );
set = vertcat(set,subSet);
%Escenario 4
camposAUtilizar = [4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,4 );
set = vertcat(set,subSet);
%Escenario 5
camposAUtilizar = [5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,5 );
set = vertcat(set,subSet);
%Escenario 6
camposAUtilizar = [6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,6 );
set = vertcat(set,subSet);
%Escenario 7
camposAUtilizar = [7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,7 );
set = vertcat(set,subSet);
%Escenario 8
camposAUtilizar = [8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,8 );
set = vertcat(set,subSet);
%Escenario 9
camposAUtilizar = [9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,9 );
set = vertcat(set,subSet);
%Escenario 10
camposAUtilizar = [10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,10 );
set = vertcat(set,subSet);
%Escenario 11
camposAUtilizar = [11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,11 );
set = vertcat(set,subSet);
%Escenario 12
camposAUtilizar = 12;
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,12 );
set = vertcat(set,subSet);
%Escenario 13
camposAUtilizar = [5,6,7,8];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,13 );
set = vertcat(set,subSet);
%Escenario 14
camposAUtilizar = [1,2,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,14 );
set = vertcat(set,subSet);
%Escenario 15
camposAUtilizar = [3,4,9,10];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,15 );
set = vertcat(set,subSet);

filename = 'resultadosData_trainlm1.xlsx';
xlswrite(filename,set)

clear('set')
algoritmo = 'trainbr';

entrenamientoRed(scoretraininput,scoretrainoutput,'trainscg',8)

%Escenario 1
camposAUtilizar = [1,2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,1);
set = subSet;
%Escenario 2
camposAUtilizar = [2,3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,2 );
set = vertcat(set,subSet);
%Escenario 3
camposAUtilizar = [3,4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,3 );
set = vertcat(set,subSet);
%Escenario 4
camposAUtilizar = [4,5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,4 );
set = vertcat(set,subSet);
%Escenario 5
camposAUtilizar = [5,6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,5 );
set = vertcat(set,subSet);
%Escenario 6
camposAUtilizar = [6,7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,6 );
set = vertcat(set,subSet);
%Escenario 7
camposAUtilizar = [7,8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,7 );
set = vertcat(set,subSet);
%Escenario 8
camposAUtilizar = [8,9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,8 );
set = vertcat(set,subSet);
%Escenario 9
camposAUtilizar = [9,10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,9 );
set = vertcat(set,subSet);
%Escenario 10
camposAUtilizar = [10,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,10 );
set = vertcat(set,subSet);
%Escenario 11
camposAUtilizar = [11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,11 );
set = vertcat(set,subSet);
%Escenario 12
camposAUtilizar = 12;
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,12 );
set = vertcat(set,subSet);
%Escenario 13
camposAUtilizar = [5,6,7,8];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,13 );
set = vertcat(set,subSet);
%Escenario 14
camposAUtilizar = [1,2,11,12];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,14 );
set = vertcat(set,subSet);
%Escenario 15
camposAUtilizar = [3,4,9,10];
subSet = probarEscenarios(datos,camposAUtilizar,algoritmo,15 );
set = vertcat(set,subSet);

filename = 'resultadosData_trainbr1.xlsx';
xlswrite(filename,set)


